# 《统计学习方法》笔记及代码更新

> lyz
>
> 2281250383@qq.com
>
> create at 2023/1/31

## finished

1. 感知机
2. logistic回归
3. KNN
4. 朴素贝叶斯(仅numpy)
5. LSTM
6. CNN
7. AdaBoost提升算法
8. K-means 聚类

## useful link

* https://github.com/LeBron-Jian/MachineLearningNote.git
* 